import 'package:flutter/material.dart';
 enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme {
  static ThemeData appThemelight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
        ),
        iconTheme: IconThemeData(color: Colors.greenAccent,
        ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: Colors.greenAccent,
    ),
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(color: Colors.greenAccent,
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Colors.greenAccent,
      ),
    );
  }
}
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}
class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme== APP_THEME.DARK
          ? MyAppTheme.appThemelight()
          :MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: () {
            setState(() {
              currentTheme== APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            },
            );
          },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.brown,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.textsms,
          // color: Colors.brown,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVidoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.brown,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.brown,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.brown,
        ),
        onPressed: () {},
      ),
      Text("Direction"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money_outlined,
          // color: Colors.brown,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("333-803-3310"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
       // color: Colors.greenAccent,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Icon(null),
    title: Text("266-440-3830"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      // color: Colors.greenAccent,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("okkostsu@okkotsu.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_pin),
    title: Text("33/3 Jujutsu school"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      // color: Colors.greenAccent,
      onPressed: () {},
    ),
  );
}

Widget buildBodyWidget() {
  return ListView(children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,

          //Height constraint at Container widget level
          height: 250,

          child: Image.network(
            "https://beebom.com/wp-content/uploads/2022/11/Pic-of-Yuta.jpg?w=640",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Yuta Okkotsu",
                  style: TextStyle(fontSize: 24),
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: Colors.grey,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Theme (
            data: ThemeData(
              iconTheme: IconThemeData(
                color: Colors.greenAccent,
              ),
            ),
            child: profileActionItem(),
  )

        ),
        Divider(
          color: Colors.grey,
        ),
        mobilePhoneListTile(),
        otherPhoneListTile(),
        Divider(
          color: Colors.grey,
        ),
        emailListTile(),
        Divider(
          color: Colors.grey,
        ),
        addressListTile(),
        Divider(
          color: Colors.grey,
        ),
      ],
    ),
  ],
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.greenAccent,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.star_border),
        color: Colors.black,
        onPressed: () {
          print("Contact is starred");
        },
      )
    ],
  );
}
Widget profileActionItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVidoCallButton(),
      buildEmailButton(),
      buildDirectionButton(),
      buildPayButton(),
    ],
  );
}